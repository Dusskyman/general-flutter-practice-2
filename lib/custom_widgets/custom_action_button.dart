import 'package:flutter/material.dart';

class CustomActionButton extends StatelessWidget {
  final Function function;
  final double width;
  final double height;
  final IconData iconData;

  const CustomActionButton({
    this.function,
    this.width,
    this.height,
    this.iconData,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).buttonColor,
      child: SizedBox(
        height: height ?? 50,
        width: width ?? 100,
        child: InkWell(
          splashColor: Theme.of(context).splashColor,
          onTap: function,
          child: Icon(iconData),
        ),
      ),
    );
  }
}
