import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CustomCounter extends StatelessWidget {
  int counter;
  CustomCounter({
    this.counter,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      color: Theme.of(context).primaryColor,
      child: Text(
        '${counter}',
        style: TextStyle(
          color: Colors.white,
          fontSize: 75,
        ),
      ),
    );
  }
}
