import 'package:flutter/material.dart';
import 'package:image_view/main.dart';

class ImagePage extends StatelessWidget {
  static const route = '/image-page';

  final List<String> imageUrl = [
    'http://wallpaperstock.net/anime-forest_wallpapers_57097_768x1024.jpg',
    'https://img.wallpapersafari.com/tablet/768/1024/86/3/jkS3le.jpg',
    'https://img.wallpapersafari.com/tablet/768/1024/26/53/0Bcv2Y.jpg',
    'https://i.pinimg.com/originals/ed/14/08/ed14084c1e4e76a3557562eb514c1da8.jpg',
    'https://mfiles.alphacoders.com/635/thumb-1920-635774.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pushReplacementNamed(context, CounterPage.route);
            },
            icon: Icon(
              Icons.arrow_circle_up,
            ),
          ),
        ),
        body: ListView(
          children: [...imageUrl.map((e) => Image.network(e)).toList()],
        ));
  }
}
