import 'package:flutter/material.dart';
import 'package:image_view/custom_widgets/custom_action_button.dart';
import 'package:image_view/custom_widgets/custom_counter.dart';
import 'package:image_view/image_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (settings) {
        if (settings.name == ImagePage.route) {
          return MaterialPageRoute(builder: (ctx) => ImagePage());
        } else {
          return MaterialPageRoute(builder: (ctx) => CounterPage());
        }
      },
      theme: ThemeData(
          primaryColor: Colors.blue,
          buttonColor: Colors.orangeAccent,
          splashColor: Colors.redAccent[200]),
      home: CounterPage(),
    );
  }
}

class CounterPage extends StatefulWidget {
  static const route = '/';
  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, ImagePage.route);
          },
          icon: Icon(
            Icons.arrow_circle_up,
          ),
        ),
      ),
      body: Align(
        alignment: Alignment.center,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            CustomCounter(
              counter: counter,
            ),
            SizedBox(
              height: 100,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomActionButton(
                  function: () {
                    setState(() {
                      counter++;
                    });
                  },
                  iconData: Icons.add,
                ),
                CustomActionButton(
                  function: () {
                    setState(() {
                      counter--;
                    });
                  },
                  iconData: Icons.remove,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
